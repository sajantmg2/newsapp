package com.example.news.utils

import androidx.room.TypeConverter
import com.example.news.data.models.Article
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*


class ArticleListTypeConverter {
    var gson = Gson()

    @TypeConverter
    fun stringToSomeObjectList(data: String?): List<Article?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<List<Article?>?>() {}.getType()
        return gson.fromJson<List<Article?>>(data, listType)
    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<Article?>?): String? {
        return gson.toJson(someObjects)
    }
}