package com.example.news.data.models


import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "news_table")
data class Article(

    @ColumnInfo(name="news_author")
    @SerializedName("author")
    val author: String?,

    @SerializedName("content")
    val content: String,

    @SerializedName("description")
    val description: String,

    @ColumnInfo(name="published_date")
    @SerializedName("publishedAt")
    val publishedAt: String?,


    @Embedded(prefix = "source_part")
    @SerializedName("source")
    val source: Source?,

    @ColumnInfo(name="news_title")
    @SerializedName("title")
    val title: String?,

    @ColumnInfo(name = "url")
    @SerializedName("url")
    val url: String?,

    @ColumnInfo(name="news_url_to_image")
    @SerializedName("urlToImage")
    val urlToImage: String?
){
    @PrimaryKey(autoGenerate = true)
    var id : Int =0

    /*@ColumnInfo(name="server_load")
    var is_saved : Int =0*/
}