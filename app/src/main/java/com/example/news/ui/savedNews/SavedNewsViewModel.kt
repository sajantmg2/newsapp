package com.example.news.ui.savedNews

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.news.data.models.Article
import com.example.news.data.repositories.NewsRepository
import kotlinx.coroutines.Job

class SavedNewsViewModel(private val repository: NewsRepository) : ViewModel() {

    private lateinit var job: Job
    private val _savedNews = MutableLiveData<Article>()
    val savedNews : LiveData<Article>
        get()=_savedNews

 /*  fun getSavedNews(context: Context){
       job=Coroutines.ioThenMain(
           {repository.getSavedNews(context)}
           {_savedNews.value=it}
       )
   }*/

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }


}
