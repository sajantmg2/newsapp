package com.example.news.ui.sources

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.news.data.models.SourceDAO
import com.example.news.data.repositories.NewsRepository
import com.example.news.utils.Coroutines
import kotlinx.coroutines.Job

class SourcesViewModel(private val repository: NewsRepository) : ViewModel() {
    private lateinit var job: Job
    private val _sources= MutableLiveData<SourceDAO>()
    val sources : LiveData<SourceDAO>
        get() = _sources

    fun getSources(){
        job = Coroutines.ioThenMain(
            { repository.getSourcesList() },
            { _sources.value = it })
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}
