package com.example.news.ui.savedNews

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.news.R
import com.example.news.data.models.Article
import com.example.news.databinding.SavedNewsReccyclerItemBinding

class SavedNewsAdapter(private val news : List<Article>
                       , private val pb : ProgressBar)
    : RecyclerView.Adapter<SavedNewsAdapter.SavedViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedViewHolder =
        SavedViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.saved_news_reccycler_item,
            parent,
            false
        )
    )

    override fun getItemCount(): Int =news.size

    override fun onBindViewHolder(holder: SavedViewHolder, position: Int) {
        println("inside saved adapter")
        holder.savedNewsReccyclerItemBinding.savedNews=news[position]
        pb.visibility= View.GONE
    }

    class SavedViewHolder(val savedNewsReccyclerItemBinding: SavedNewsReccyclerItemBinding)
        : RecyclerView.ViewHolder(savedNewsReccyclerItemBinding.root)

}