package com.example.news.ui.newsFromSource

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.news.data.models.NewsDAO
import com.example.news.data.repositories.NewsRepository
import com.example.news.utils.Coroutines
import kotlinx.coroutines.Job

class NewsFromSourceViewModel(private val repository: NewsRepository) : ViewModel() {

    private lateinit var job: Job
    private val _sourceNews= MutableLiveData<NewsDAO>()
    val sourceNews : LiveData<NewsDAO>
        get() = _sourceNews

    fun getNewsFromSource(sId : String?){
        job = Coroutines.ioThenMain(
            { repository.getNewsFromSource(sId) },
            { _sourceNews.value = it })
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }


}
