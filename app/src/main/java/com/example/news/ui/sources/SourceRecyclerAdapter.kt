package com.example.news.ui.sources

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.example.news.R
import com.example.news.data.models.Source
import com.example.news.databinding.SourcesRecyclerItemBinding


class SourceRecyclerAdapter(
    private val sources : List<Source>, private val navController: NavController
    ) : RecyclerView.Adapter<SourceRecyclerAdapter.SourcesViewHolder>(){


    inner class SourcesViewHolder(
        val sourcesRecyclerItemBinding: SourcesRecyclerItemBinding)
        :RecyclerView.ViewHolder(sourcesRecyclerItemBinding.root){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SourcesViewHolder =
        SourcesViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.sources_recycler_item,
                parent,
                false)
    )

    override fun getItemCount(): Int = sources.size

    override fun onBindViewHolder(holder: SourcesViewHolder, position: Int) {
        holder.sourcesRecyclerItemBinding.source=sources[position]
        holder.sourcesRecyclerItemBinding.root.setOnClickListener(){
            val sourceId : String = sources[position].id!!
            var bundle = bundleOf("sourceId" to sourceId)
            navController.navigate(R.id.newsFromSourceFragment, bundle)

                //val action = SourcesFragmentDirections.sourceClicked(sourceId)
            //navController.navigate(action)
        }
    }
}


