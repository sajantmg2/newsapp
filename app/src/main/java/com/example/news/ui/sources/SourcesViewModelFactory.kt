package com.example.news.ui.sources

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.news.data.repositories.NewsRepository

@Suppress("UNCHECKED_CAST")
class SourcesViewModelFactory(private val repository: NewsRepository)
    : ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SourcesViewModel(repository) as T
    }
}