package com.example.news.ui.newsFromSource

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.news.R
import com.example.news.data.db.NewsDatabase
import com.example.news.data.models.Article
import com.example.news.data.network.NewsApi
import com.example.news.data.repositories.NewsRepository
import com.example.news.ui.BaseFragment
import com.example.news.ui.toast
import com.example.news.ui.topNews.TopNewsRecyclerAdapter
import kotlinx.android.synthetic.main.top_head_lines_us_fragment.*
import kotlinx.coroutines.launch

class NewsFromSourceFrag : BaseFragment() {

    companion object {
        fun newInstance() =
            NewsFromSourceFrag()
    }

    private lateinit var factory: NewsFromSourceViewModelFactory
    private lateinit var viewModel: NewsFromSourceViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.top_head_lines_us_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val pb : ProgressBar=this.loading_bar

        //val safeArgs : NewsFromSourceFragArgs by navArgs()
        //val sourceId = safeArgs.sID
        val sourceId=arguments?.getString("sourceId")

        val api: NewsApi = NewsApi()
        val repository= NewsRepository(api)
        factory=
            NewsFromSourceViewModelFactory(
                repository
            )
        viewModel=ViewModelProviders.of(this,factory).get(NewsFromSourceViewModel::class.java)
        viewModel.getNewsFromSource(sourceId)

        viewModel.sourceNews.observe(viewLifecycleOwner, Observer { sourceNews ->
            top_news_us_recycler_view.also {
                //it.layoutManager= StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
                it.layoutManager = GridLayoutManager(requireContext(),2)
                it.setHasFixedSize(true)
                it.adapter =null
                    TopNewsRecyclerAdapter(sourceNews.articles,pb,requireContext(),object : TopNewsRecyclerAdapter.ViewClickListener{
                        override fun onVClick(article: Article) {
                            launch {
                                context.let {
                                    NewsDatabase(requireContext()).getNewsDbDao().addNews(article)
                                    if (it != null) {
                                        it.toast("News saved.")
                                    }
                                }
                            }
                        }
                    })
            }
        })
    }

}
